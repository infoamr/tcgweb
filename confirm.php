<!doctype html>
<?php //echo '<pre>'; print_r($_POST); exit; 
?>
<html lang="en" class="no-js">
<head>
	<title>Book Your CSCS & CITB Health, Safety & Environment Test Online | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="canonical" href="https://www.theconstructionguide.co.uk" />

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">

			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<p><i class="material-icons">phone</i><a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i><a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
						
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.php">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="page-banner-section">
			<div class="container">
				<h1>Summary</h1>
				<ul class="page-depth">
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Summary</a></li>
				</ul>
			</div>
		</section>
		<!-- End page-banner-section -->

		<!-- cart-section 
			================================================== -->
		<section class="cart-section">
			<div class="container">
				<div class="row">
					
					<div class="col-lg-8">
						<div class="sidebar">
							<div class="widget cart-widget">
							    <form class="billing-details" action="cart.php" method="post">
								<h2>Your Order</h2>
								<table>
									<tbody>
									    <tr>
											<td style="width: 50%;"><?php 
											        $item=explode('|', $_POST['item']);
                                                    echo $item[0];?>
                                            </td>
										</tr>
										<tr>
											<th class="name-pro">Name</th>
											<td><?php echo $_POST['fname'].' '.$_POST['lname'];?></td>
										</tr>
										<tr>
											<th class="name-pro">Email</th>
											<td><?php echo $_POST['email']; ?></td>
										</tr>
										<tr>
											<th class="name-pro">Phone</th>
											<td><?php echo $_POST['phone']; ?></td>
										</tr>
										<tr>
											<th class="name-pro">Address</th>
											<td><?php echo $_POST['address']; ?></td>
										</tr>
										<tr>
											<th class="name-pro">City</th>
											<td><?php echo $_POST['city']; ?></td>
										</tr>
										<tr>
											<th class="name-pro">State</th>
											<td><?php echo $_POST['state']; ?></td>
										</tr>
										<tr>
											<th class="name-pro">Postcode</th>
											<td><?php echo $_POST['postcode']; ?></td>
										</tr>
										<!--<tr>-->
										<!--	<th class="name-pro">Cost</th>-->
										<!--	<td>-->
											    <?php //$cost=explode('|', $_POST['item']); echo $cost[1]; ?>
                                        <!--    </td>-->
										<!--</tr>-->
									</tbody>
								</table>
								<input type="hidden" name="number_of_bookings" value="1">
                                <input type="hidden" name="test_type" value="<?php echo $item[0]; ?>">
                                <input type="hidden" name="testprice" value="<?php echo $item[1]; ?>">
                                <input type="hidden" name="first_name" value="<?php echo $_POST['fname']; ?>">
                                <input type="hidden" name="last_name" value="<?php echo $_POST['lname']; ?>">
                                <input type="hidden" name="email" value="<?php echo $_POST['email']; ?>">
                                <input type="hidden" name="mobile" value="<?php echo $_POST['phone']; ?>">
                                <input type="hidden" name="address" value="<?php echo $_POST['address']; ?>">
                                <input type="hidden" name="address2" value="<?php echo $_POST['address2']; ?>">
                                <input type="hidden" name="town" value="<?php echo $_POST['city']; ?>">
                                <input type="hidden" name="state" value="<?php echo $_POST['state']; ?>">
                                <input type="hidden" name="postcode" value="<?php echo $_POST['postcode']; ?>">
                                <button type="submit" class="btn btn-warning" style="border-radius: 25px; padding:10px 25px 10px 25px;">Proceed to Pay</button>
                                </form>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="sidebar">

							<div class="ads-widget widget">
								<a href="#">
									<img src="upload/blog/clipboard-image.png" alt="">
								</a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End cart section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p>We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="#" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src="js/script.js"></script>
	
	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>
<?php
    
    include("connect.php");
    
    $firstname=$_POST["fname"];
    $lastname=$_POST["lname"];
    $email=$_POST["email"];
    $phone=$_POST['phone'];
    $address=$_POST['address'];
    $address2=$_POST['address2'];
    $town=$_POST['city'];
    $state=$_POST['state'];
    $postcode=$_POST['postcode'];
    $addinfo=$_POST['addinfo'];
    
    $cus = "INSERT INTO customers (firstname,lastname,email,phone,address,address2,town,state,postcode)
    VALUES ('$firstname','$lastname','$email','$phone','$address','$address2','$town','$state','$postcode')";
    if (mysqli_query($conn, $cus)) {
    //	echo "New record created successfully !";
    } else {
    //	echo "Error: " . $sql . "" . mysqli_error($conn);
    }
    
    $sql ="SELECT * from customers ORDER BY id DESC LIMIT 1";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) 
    {
        while($row = $result->fetch_assoc()) 
        {   
            $customer_id=$row['id'];
        }
    }
    
    $item_name=$item[0];
    $price=$item[1];
    $order_id = 'TCG'.mt_rand(100000,999999);
      
    $sql = "INSERT INTO orders (order_id,customer_id,itemname,price,add_info)
    VALUES ('$order_id','$customer_id','$item_name','$price','$addinfo')";
    if (mysqli_query($conn, $sql)) {
    //	echo "New record created successfully !";
    } else {
    //	echo "Error: " . $sql . "" . mysqli_error($conn);
    }
    
    $sqlorder ="SELECT * from orders ORDER BY id DESC LIMIT 1";
    $res = $conn->query($sqlorder);
    if ($res->num_rows > 0) 
    {
        while($r = $res->fetch_assoc()) 
        {   
            $order_id=$r['order_id'];
            $created_at= date('M j Y g:i A', strtotime($r['created_at']));
        }
    }
    $conn->close();

$message = '<html>
<title>The Construction Guide</title>
<link rel="icon" href="https://theconstructionguide.co.uk/images/favicon.ico" type="image/gif" sizes="16x16">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css"/>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet" type="text/css"/>
<body style="background-color:#1c3730;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
  <table style="max-width:700px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;">
    <thead>
      <tr>
        <th style="text-align:left;"><img style="max-width: 150px;" src="https://theconstructionguide.co.uk/images/logo.png" alt="tcg"></th>
        <th style="text-align:right;font-size:12px;color:#1c3730;">25 Sipson Road<br>West Drayton, London<br>Postcode:UB7 9DQ<br>Phone:0800-046-5506<br>info@theconstructionguide.co.uk</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="height:25px;"></td>
      </tr>
      <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
          <p style="font-size:14px;margin:0 0 0 0;">
          <span style="font-weight:bold;display:inline-block;width:75%">Order Date</span>
          <span style="font-weight:bold;display:inline-block;width:24%; text-align:right">'.$created_at.'</span></p>
          <p style="font-size:14px;margin:0 0 0 0;">
          <span style="font-weight:bold;display:inline-block;width:75%">Order ID</span>
          <span style="font-weight:bold;display:inline-block;width:24%; text-align:right">'.$order_id.'</span></p>
          <p style="font-size:14px;margin:0 0 0 0;">
          <span style="font-weight:bold;display:inline-block;width:75%">Order Amount</span>';

		$message .= '<span style="font-weight:bold;display:inline-block;width:24%; text-align:right">£ '.$price.'</span></p>';
          
        $message .= '</td>
      </tr>
      <tr>
        <td style="height:25px;"></td>
      </tr>
      <tr>
        <td style="width:50%;padding:20px;vertical-align:top">
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:12px">Name</span>  '.$firstname.' '.$lastname.'</p>
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:12px;">Email</span> '.$email.'</p>
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:12px;">Phone</span> '.$phone.'</p>
          <p style="margin:0 0 10px 0;padding:0;font-size:14px;"><span style="display:block;font-weight:bold;font-size:12px;">Address</span> '.$address1.' '.$address2.' '.$town.' '.$postcode.'</p>
        </td>
      </tr>';

      $message .= '<tr>
        <td colspan="2" style="font-weight:bold;font-size:20px;text-align:center;color:#1c3730;">Details</td>
      </tr>';

      $message .= '<tr>
        <td colspan="2">
          <p style="font-size:14px;margin:0 0 0 0;">
          <span style="font-weight:bold;display:inline-block;width:50%; text-align:left;font-size:12px;font-weight:bold; line-height:3">Name</span> 
          <span style="font-weight:bold;display:inline-block;width:46%; font-size:12px;font-weight:normal;text-align:left; line-height:3; padding-left:10px">'.$item_name.'</span>
          <span style="font-weight:bold;display:inline-block;width:50%; text-align:left;font-size:12px;font-weight:bold; line-height:3">Cost</span> 
          <span style="font-weight:bold;display:inline-block;width:46%; font-size:12px;font-weight:normal;text-align:left; line-height:3; padding-left:10px">£ '.$price.'</span>
          </p>
        </td>
      </tr>

    </tbody>
    <tfooter>
      <tr>
        <td style="height:25px;"></td>
      </tr>
      <tr>
        <td colspan="2" style="border: solid 1px #ddd;">
          <img src="https://theconstructionguide.co.uk/images/logo.png" width="100%" alt="bn">
        </td>
      </tr>
      <tr>
        <td style="height:25px;"></td>
      </tr>
      <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
          <p>All sales are in accordance with our terms and conditions, see here<br><a href="https://theconstructionguide.co.uk/terms.php" target="_blank">https://theconstructionguide.co.uk/terms.php</a></p>
		  <p style="padding-top: 20px">ChooseURFood LTD T/A <br> The Construction Guide <br>Registered Office:76 Coronation Street Salford M5 3SA UK</p>
          <p style="text-align: center; font-size:12px;margin:0 0 0 0;"><a href="https://theconstructionguide.co.uk/" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #0080bf; background-color: transparent; border-radius: 28px; -webkit-border-radius: 28px; -moz-border-radius: 28px; width: auto; width: auto; border-top: 1px solid #0080bf; border-right: 1px solid #0080bf; border-bottom: 1px solid #0080bf; border-left: 1px solid #0080bf; padding-top: 5px; padding-bottom: 5px; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;"><span style="font-size: 12px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;">Thank You</span></span></a> </p>
        </td>
      </tr>
      <tr>
        <td style="height:25px;"></td>
      </tr>
      <tr>
        <td colspan="2" style="border: solid 1px #ddd; padding:10px 20px;">
          <p style="text-align: center;font-size:12px;margin:0 0 6px 0;"><span style="font-weight:normal;display:inline-block;">Need Help?<a href="https://theconstructionguide.co.uk/contact.php">Click</a> to contact us for queries</span></p>
          <p style="text-align: center;font-size:12px;margin:0 0 0 0;">
          <a href="https://www.facebook.com/The-Construction-Guide-100481089111368" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #0080bf; background-color: transparent; border-radius: 28px; -webkit-border-radius: 28px; -moz-border-radius: 28px; width: auto; width: auto; border-top: 1px solid #0080bf; border-right: 1px solid #0080bf; border-bottom: 1px solid #0080bf; border-left: 1px solid #0080bf; padding-top: 5px; padding-bottom: 5px; mso-border-alt: none; word-break: keep-all;" target="_blank"><img height="25px;" width="25px;" src="https://theconstructionguide.co.uk/images/facebook2x.png" alt="ln"></a>
          <a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #0080bf; background-color: transparent; border-radius: 28px; -webkit-border-radius: 28px; -moz-border-radius: 28px; width: auto; width: auto; border-top: 1px solid #0080bf; border-right: 1px solid #0080bf; border-bottom: 1px solid #0080bf; border-left: 1px solid #0080bf; padding-top: 5px; padding-bottom: 5px; mso-border-alt: none; word-break: keep-all;" target="_blank"><img height="25px;" width="25px;" src="https://theconstructionguide.co.uk/images/linkedin2x.png" alt="ln"></a>
          <a href="https://twitter.com/constru47515413" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #0080bf; background-color: transparent; border-radius: 28px; -webkit-border-radius: 28px; -moz-border-radius: 28px; width: auto; width: auto; border-top: 1px solid #0080bf; border-right: 1px solid #0080bf; border-bottom: 1px solid #0080bf; border-left: 1px solid #0080bf; padding-top: 5px; padding-bottom: 5px; mso-border-alt: none; word-break: keep-all;" target="_blank"><img height="25px;" width="25px;" src="https://theconstructionguide.co.uk/images/twitter2x.png" alt="ln"></a> </p>
        </td>
      </tr>
    </tfooter>
  </table>

</body>
</html>';
    
    $toEmail = "info@theconstructionguide.co.uk";
    $subject = 'Registration Successful! Payment need to verify - '.$item_name.' ('.$order_id.')';
    
    $mailHeaders = "From: " . $firstname. "<". $email.">\r\n";
    $mailHeaders  = 'MIME-Version: 1.0' . "\r\n";
    $mailHeaders .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
    if(mail($toEmail,$subject,$message,$mailHeaders))
    {
        $sub="The Construction Guide Order ".$order_id;
        
        $headers = "From: 'The Construction Guide'<'info@theconstructionguide.co.uk'>\r\n";
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        if(mail($email,$sub,$message,$headers,'-f info@theconstructionguide.co.uk'))
        {
            
        } 
    } 
?>