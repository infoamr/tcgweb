<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Book Your CSCS & CITB Health, Safety & Environment Test Online | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="canonical" href="https://www.theconstructionguide.co.uk" />

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">

			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p><i class="material-icons">phone</i><a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i><a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
						
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Health & Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.html">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="page-banner-section">
			<div class="container">
				<h1>One Day Health & Safety Awareness Course</h1>
				<ul class="page-depth">
					<li><a href="index.php">Home</a></li>
					<li><a href="courses.php">Courses</a></li>
					<li><a href="oneday-course.php">One Day Health & Safety Awareness Course</a></li>
				</ul>
			</div>
		</section>
		<!-- End page-banner-section -->

		<!-- single-course-section 
			================================================== -->
		<section class="single-course-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">

						<div class="single-course-box">

							<!-- single top part -->
							<div class="product-single-top-part">
								<div class="product-info-before-gallery">
									<div class="course-category before-gallery-unit">
										<div class="icon">
											<i class="material-icons">bookmark_border</i>
										</div>
										<div class="info">
											<span class="label">Category</span>
											<div class="value">
												<a href="courses.php">Safety Courses</a>
											</div>
										</div>
									</div>
									
								</div>
								<!--<div class="course-single-gallery">-->
								<!--	<img src="upload/courses/4.jpg" alt="">-->
								<!--</div>-->

							</div>

							<!-- single course content -->
							<div class="single-course-content">
								<h2>Why should you do the 1 day site safety plus health and safety Course</h2>
								<p style="text-align:justify">As defined by the World Health Organization (WHO), Health is a complete state
								of fitness of physical, mental, and social well-being. For more knowledge, it will be good for people to have a
								look at health and safety awareness training. Every worker and supervisor is eligible to take this training under
								the Occupational Health and Safety Act.
								
								It is important to take good care of your mental, physical and social health throughout your life.
								Taking care of your health is a lifelong process that involves all the practices and awareness for staying healthy.
								Safety means you are being protected by some non-desirable outcomes or by any damage.
								When we label something as “safe” it generally means that it is protected from certain parameters or has some boundaries.</p>
								
								
								<h2>Explain What is 1 Day Site Safety plus Health and Safety Awareness Course?</h2>
								<p style="text-align:justify">Under the Act 1974 ‘Health and Safety at Work’, Employers are imposed to make sure that employees are provided with enough training and information to lessen the risk at work. The training method is up to the employers. In case of an emergency, generally, professionals come to provide you the safety training. Organizations have their own team for disaster management who provides safety measures and works to over the risk of safety for their employees. Health management is also taken care of by the employers and it’s their responsibility to make sure every employee is fit and fine. Health and safety are provided to all the workers under the Occupational Health and Safety Act. It is done to explain to the workers their rights and responsibilities. It also provides a basic introduction to disaster management with the best precautions techniques.</p>
								<h2>Why is it require?</h2>
								<p>This one-day Health and Safety Awareness course is required for better protection of yourself from the working surroundings.</p>
                                <!-- course section -->
								<div class="course-section">
									<h6>It covers the following things:-</h6>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Ill-health prevention and the planning process for the casualty.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Company’s Health and Safety policy.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6">Company’s agenda, procedures, practices, and resources for implementing and establishing, maintaining, and reviewing the occupational health and safety policy.</p>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								<p>The management is supposed to cover and should cover the entire plan and tactic of an employee’s occupational safety and health in the organization.</p>
								<h2>What are the criteria required?</h2>
								<p style="text-align:justify">There are no such criteria required for both employer and the employee to get the training for Safety and Health Awareness course. Every individual in the workplace has some or the other part to play in health and safety, therefore they require training. Training should be mandatory for all levels of employers and employees to have a better understanding of the organization’s policy. It is important to complete off the training task systematically, hence, any senior member of the organization should be present during the training.</p>
                                <h2>How to apply?</h2>
								<p style="text-align:justify">There are plenty of ways to apply for these health and safety courses and it is up to the organization how they plan it. Generally, it is available on the internet where companies are providing a one-day training course for employees and laborers too where all the required parts would be covered with giving a practical demonstration.</p>
								<!-- end course section -->
							</div>
							<!-- end single course content -->
						</div>
                        <a class="button-two" href="https://theconstructionguide.co.uk/applycourse.php?id=4">Apply Now</a>
					</div>
					<div class="col-lg-4">
						<div class="sidebar"><div class="ads-widget widget">
								<a href="#">
									<img src="upload/blog/clipboard-image.png" alt="">
								</a>
							</div>
						    
							
							
						</div>
					</div>

				</div>
						
			</div>
		</section>
		<!-- End single-course section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p>We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
							
							
						    
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src="js/script.js"></script>

	<script>
		var tpj=jQuery;
		var revapi202;
		tpj(document).ready(function() {
			if (tpj("#rev_slider_202_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_202_1");
			} else {
				revapi202 = tpj("#rev_slider_202_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "js/",
					dottedOverlay: "none",
					delay: 5000,
					navigation: {
						keyboardNavigation: "off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation: "off",
						onHoverStop: "off",
						arrows: {
					        enable: true,
					        style: 'gyges',
					        left: {
					            container: 'slider',
					            h_align: 'left',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        },
					 
					        right: {
					            container: 'slider',
					            h_align: 'right',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        }
					    },
						touch: {
							touchenabled: "on",
							swipe_threshold: 75,
							swipe_min_touches: 50,
							swipe_direction: "horizontal",
							drag_block_vertical: false
						},
						bullets: {
 
					        enable: false,
					        style: 'persephone',
					        tmp: '',
					        direction: 'horizontal',
					        rtl: false,
					 
					        container: 'slider',
					        h_align: 'center',
					        v_align: 'bottom',
					        h_offset: 0,
					        v_offset: 55,
					        space: 7,
					 
					        hide_onleave: false,
					        hide_onmobile: false,
					        hide_under: 0,
					        hide_over: 9999,
					        hide_delay: 200,
					        hide_delay_mobile: 1200
 						}
					},
					responsiveLevels: [1210, 1024, 778, 480],
					visibilityLevels: [1210, 1024, 778, 480],
					gridwidth: [1210, 1024, 778, 480],
					gridheight: [700, 700, 600, 600],
					lazyType: "none",
					parallax: {
						type: "scroll",
						origo: "slidercenter",
						speed: 1000,
						levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
						type: "scroll",
					},
					shadow: 0,
					spinner: "off",
					stopLoop: "off",
					stopAfterLoops: -1,
					stopAtSlide: -1,
					shuffle: "off",
					autoHeight: "off",
					fullScreenAutoWidth: "off",
					fullScreenAlignForce: "off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "0px",
					disableProgressBar: "on",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll: "off",
						nextSlideOnWindowFocus: "off",
						disableFocusListener: false,
					}
				});
			}
		}); /*ready*/
	</script>	

	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>