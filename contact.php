<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Book Your CSCS & CITB Health, Safety & Environment Test Online | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="canonical" href="https://www.theconstructionguide.co.uk" />

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">

			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<p><i class="material-icons">phone</i> <a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i> <a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
						
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.php">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- map section -->
		<div id="rev_slider_202_1_wrapper">
		    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d39737.867219756845!2d-0.4656930000000001!3d51.501901000000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876721c9c04ffff%3A0xac56203085bd95e2!2sCITB!5e0!3m2!1sen!2sus!4v1635434459162!5m2!1sen!2sus" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
		<!-- end map section -->

		<!-- contact-section 
			================================================== -->
		<section class="contact-section">
			<div class="container">
				<div class="contact-box">
					<h1>Get in Touch</h1>
					<p>Someone finds it difficult to understand your creative idea? There is a better visualisation. Share your views with us, we’re looking forward to hear from you.</p>
					<form id="contact-form" action="enquiry.php" method="post">
						<div class="row">
							<div class="col-md-6">
								<label for="name">Your Name (required)</label>
								<input name="name" id="name" type="text" required>
							</div>
							<div class="col-md-6">
								<label for="email">Your Email (required)</label>
								<input name="email" id="email" type="text" required>
							</div>
						</div>
						<label for="phone">Your Phone Number</label>
						<input name="phone" id="phone" type="text">
						<label for="comment">Your Message (required)</label>
						<textarea name="comment" id="comment" required></textarea>
						<div class="g-recaptcha" data-sitekey="6LdccMQbAAAAAOS9eH9-J3Ng_lUma82261vFgYil" data-callback="verifyCaptcha"></div>
                        <div id="g-recaptcha-error"></div>
                        <br>
						<button type="submit">Submit Message</button>
						<div id="msg" class="message"></div>
					</form>
				</div>
			</div>
		</section>
		<!-- End contact section -->

		<!-- contact-info-section 
			================================================== -->
		<section class="contact-info-section">
			<div class="container">
				<div class="contact-info-box">
					<div class="row">

						<div class="col-lg-4 col-md-6">
							<div class="info-post">
								<div class="icon">
									<i class="fa fa-envelope-o"></i>
								</div>
								<div class="info-content">
									<p>
										Tel: <a href="tel:0800-046-5506">0800-046-5506</a> <br>
										Mail: <a href="info@theconstructionguide.co.uk">info@theconstructionguide.co.uk</a>
									</p>
								</div>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="info-post">
								<div class="icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-content">
									<p>
										25 Sipson Road, West Drayton, <br>
										UB7 9DQ, London
									</p>
								</div>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="info-post">
								<div class="icon">
									<i class="fa fa-clock-o"></i>
								</div>
								<div class="info-content">
									<p>
										Our office is open:<br>
										Mon to Fri from 8am to 6pm
									</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- End contact-info section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p style="text-align:justify">We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
							
							
						    
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	
	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script> 
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
	<script src="js/script.js"></script>

	<script>
		var tpj=jQuery;
		var revapi202;
		tpj(document).ready(function() {
			if (tpj("#rev_slider_202_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_202_1");
			} else {
				revapi202 = tpj("#rev_slider_202_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "js/",
					dottedOverlay: "none",
					delay: 5000,
					navigation: {
						keyboardNavigation: "off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation: "off",
						onHoverStop: "off",
						arrows: {
					        enable: true,
					        style: 'gyges',
					        left: {
					            container: 'slider',
					            h_align: 'left',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        },
					 
					        right: {
					            container: 'slider',
					            h_align: 'right',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        }
					    },
						touch: {
							touchenabled: "on",
							swipe_threshold: 75,
							swipe_min_touches: 50,
							swipe_direction: "horizontal",
							drag_block_vertical: false
						},
						bullets: {
 
					        enable: false,
					        style: 'persephone',
					        tmp: '',
					        direction: 'horizontal',
					        rtl: false,
					 
					        container: 'slider',
					        h_align: 'center',
					        v_align: 'bottom',
					        h_offset: 0,
					        v_offset: 55,
					        space: 7,
					 
					        hide_onleave: false,
					        hide_onmobile: false,
					        hide_under: 0,
					        hide_over: 9999,
					        hide_delay: 200,
					        hide_delay_mobile: 1200
 						}
					},
					responsiveLevels: [1210, 1024, 778, 480],
					visibilityLevels: [1210, 1024, 778, 480],
					gridwidth: [1210, 1024, 778, 480],
					gridheight: [700, 700, 600, 600],
					lazyType: "none",
					parallax: {
						type: "scroll",
						origo: "slidercenter",
						speed: 1000,
						levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
						type: "scroll",
					},
					shadow: 0,
					spinner: "off",
					stopLoop: "off",
					stopAfterLoops: -1,
					stopAtSlide: -1,
					shuffle: "off",
					autoHeight: "off",
					fullScreenAutoWidth: "off",
					fullScreenAlignForce: "off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "0px",
					disableProgressBar: "on",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll: "off",
						nextSlideOnWindowFocus: "off",
						disableFocusListener: false,
					}
				});
			}
		}); /*ready*/
	</script>	

	<script>
        var recaptcha_response = '';
        function submitUserForm() {
            if(recaptcha_response.length == 0) {
                document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">This field is required.</span>';
                return false;
            }
            return true;
        }
         
        function verifyCaptcha(token) {
            recaptcha_response = token;
            document.getElementById('g-recaptcha-error').innerHTML = '';
        }
    </script>
    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->

</body>
</html>