<?php 
ini_set('display_errors', 0);
$toEmail = "info@theconstructionguide.co.uk";
// $email = $_POST["email"];

include("connect.php");

if(isset($_POST['submit'])){
    $email=$_POST["email"];
}
  
$sql = "INSERT INTO newsletter (email) VALUES ('$email')";
if (mysqli_query($conn, $sql)) {
//	echo "New record created successfully !";
} else {
//	echo "Error: " . $sql . "" . mysqli_error($conn);
}

$conn->close();

$sub = 'Newsletter Subscription';
$msgbody = 'Thank you for subscribing us.<br>We will get back to you shortly.';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';

require_once "vendor/autoload.php";

//PHPMailer Object
$mail = new PHPMailer(true); 

//From email address and name
$mail->From = $toEmail;
$mail->FromName = 'The Construction Guide';

//To address and name
$mail->addAddress($email); //Recipient name is optional

//Send HTML or Plain Text email
$mail->isHTML(true);

$mail->Subject = $sub;
$mail->Body = $msgbody;

try {
    $mail->send();
    //echo "Message has been sent successfully";
} catch (Exception $e) {
    //echo "Mailer Error: " . $mail->ErrorInfo;
}

$htmlContent = ' 
    <html> 
    <head> 
        <title>The Construction Guide</title> 
    </head> 
    <body> 
        <h1>Newsletter Inquiry!</h1> 
        <table cellspacing="0" style="border: 2px dashed #FB4314; width: 100%;">
            <tr style="background-color: #e0e0e0;"> 
                <th>Email:</th><td>'.$email.'</td> 
            </tr> 
        </table> 
    </body> 
    </html>'; 

// Set content-type header for sending HTML email 
$headers = "MIME-Version: 1.0" . "\r\n"; 
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n"; 
 
// Additional headers 
$headers .= 'From: '.$name.'<'.$email.'>' . "\r\n";

$subject = 'Newsletter Inquiry';
$mail_status = mail($toEmail,$subject,$htmlContent,$headers);

if ($mail_status) { ?>
 <script language="javascript" type="text/javascript">
 alert('Thank you for subscribing us.');
  window.location.href = 'https://theconstructionguide.co.uk/';
 </script>
 <?php
 } else { ?>
  <script language="javascript" type="text/javascript">
// alert('Message sending failed');
   window.location.href = 'https://theconstructionguide.co.uk/';
  </script>
<?php } ?>