function validateInput(form,name){
  if (!!form.querySelector('[name='+name+']')){
    if(form.querySelector('[name='+name+']').value){
      return true;
    }
    else {
      return false;
    }
  }
  else
  {
    return false;
  }
}
function validateCheckbox(form,name){
 if (!!form.querySelector('[name='+name+']')){
  if(form.querySelector('[name='+name+']').checked){
    return true;
  }
  else {
    return false;
  }
}
else
{
  return false;
}
}
function stripInvalidChars(form){
var elements = form.elements;
for (var i = 0, element; element = elements[i++];) {
    element.value = element.value.replace("'","");
}
}
/**
 * [validateForm Validates form and ensures all inputs are entered]
 * @method validateForm
 * @param  {[Object]}     cartForm
 * @param  {[String]}     intType
 * @param  {[String]}     [full=null]
 * @return {[Boolean]}     [False is any of the fields inputs are blank or missing]
 */
function validateForm(cartForm,intType,full){
  var form_errors = document.getElementById("form_errors");
  var errors =[];
  stripInvalidChars(cartForm);//add in a function to strip invalid chars on submit
  switch (intType) {
    case 'hosted':
      validateInput(cartForm,'FullAmount') ? null : errors.push("Amount asdasdasdis missing");
      validateInput(cartForm,'OrderID') ? null : errors.push("Order ID is missing");
      validateInput(cartForm,'OrderDescription') ? null : errors.push("Order Description is missing");
      validateCheckbox(cartForm,'termsandconditions') ? null : errors.push("Please accept the terms and conditions before proceeding");
      if (full=="true"){
        validateInput(cartForm,'CustomerName') ? null : errors.push("Customer Name is missing");
        validateInput(cartForm,'Address1') ? null : errors.push("Address is missing");
        validateInput(cartForm,'City') ? null : errors.push("City is missing");
        validateInput(cartForm,'State') ? null : errors.push("State is missing");
        validateInput(cartForm,'PostCode') ? null : errors.push("Post Code is missing");
        validateInput(cartForm,'Country') ? null : errors.push("Country is missing");
      }
      break;
    default:
  }
  if (errors.length>0){
    var error_warnings="<p class='error_title'>Errors have been found in the form</p>";
    for (error in errors){
      error_warnings += '<p>' + errors[error] + '</p>';
    }
    form_errors.innerHTML = error_warnings;
    return false;
  }
  else{
    return true;
  }
}
