<!doctype html>
<html lang="en" class="no-js">
<head>
	<title>Study Material | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="canonical" href="https://www.theconstructionguide.co.uk" />
</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">

			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<p><i class="material-icons">phone</i> <a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i> <a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
						
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				<div class="shopping-cart-box">
					<a class="shop-icon" href="cart.html">
						<i class="material-icons">shopping_cart</i>
						Cart
						<span class="studiare-cart-number">0</span>
					</a>
				</div>
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.php">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="page-banner-section">
			<div class="container">
				<h1>Study Material</h1>
				<ul class="page-depth">
					<li><a href="index.php">Home</a></li>
					<li><a href="studymaterial.php">Study Material</a></li>
				</ul>
			</div>
		</section>
		<!-- End page-banner-section -->

		<!-- courses-section 
			================================================== -->
		<section class="courses-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="courses-top-bar">
							<div class="courses-view">
								<a href="studymaterial.php" class="grid-btn">
									<i class="fa fa-th-large" aria-hidden="true"></i>
								</a>
								<span>Showing results</span>
							</div>
							<form class="search-course">
								<input type="search" name="search" id="search_course" placeholder="Search..." />
								<button type="submit">
									<i class="material-icons">search</i>
								</button>
							</form>
						</div>

						<div class="course-post list-style">
							<div class="course-thumbnail-holder">
								<a href="#">
									<img src="images/study_opr_book.jpg" alt="">
								</a>
							</div>
							<div class="course-content-holder">
								<div class="course-content-main">
									<h2 class="course-title">
										<a href="#">Health, Safety and Environment Test for Operatives and Specialists Revision Book</a>
									</h2>
									<p>This book is aimed at anyone who is about to take the CSCS CITB Touchscreen Operatives or Specialists Test.</p>
								</div>
								<div class="course-content-bottom">
    								<div class="star-rating has-ratings" title="">
    									<span style="width:100%">
    										<button type="button" class="btn btn-sm btn-success" disabled>£20.00</button>
    									</span>
    								</div>
    								<a href="<?php echo 'applystdmtl.php?id=oprbk'; ?>" class="btn btn-sm btn-warning right-part">Buy Now</a>
    							</div>
							</div>
						</div>

						<div class="course-post list-style">
							<div class="course-thumbnail-holder">
								<a href="#">
									<img src="images/study_opr.jpg" alt="">
								</a>
							</div>
							<div class="course-content-holder">
								<div class="course-content-main">
									<h2 class="course-title">
										<a href="#">Health, Safety and Environment Test for Operatives and Specialists Revision DVD</a>
									</h2>
									<p>This book is aimed at anyone who is about to take the CSCS CITB Touchscreen Operatives or Specialists Test.</p>
								</div>
								<div class="course-content-bottom">
    								<div class="star-rating has-ratings" title="">
    									<span style="width:100%">
    										<button type="button" class="btn btn-sm btn-success" disabled>£20.00</button>
    									</span>
    								</div>
    								<a href="<?php echo 'applystdmtl.php?id=oprdvd'; ?>" class="btn btn-sm btn-warning right-part">Buy Now</a>
    							</div>
							</div>
						</div>

						<div class="course-post list-style">
							<div class="course-thumbnail-holder">
								<a href="#">
									<img src="images/study_mgr.jpg" alt="">
								</a>
							</div>
							<div class="course-content-holder">
								<div class="course-content-main">
									<h2 class="course-title">
										<a href="#">Health, Safety and Environment Test for Managers and Professionals Revision Book</a>
									</h2>
									<p>This book is aimed at anyone who is about to take the CSCS CITB Touchscreen Managers and Professionals Test.</p>
								</div>
								<div class="course-content-bottom">
    								<div class="star-rating has-ratings" title="">
    									<span style="width:100%">
    										<button type="button" class="btn btn-sm btn-success" disabled>£20.00</button>
    									</span>
    								</div>
    								<a href="<?php echo 'applystdmtl.php?id=mgrbk'; ?>" class="btn btn-sm btn-warning right-part">Buy Now</a>
    							</div>
							</div>
						</div>
						
						<div class="course-post list-style">
							<div class="course-thumbnail-holder">
								<a href="#">
									<img src="images/study_mgr_dvd.jpg" alt="">
								</a>
							</div>
							<div class="course-content-holder">
								<div class="course-content-main">
									<h2 class="course-title">
										<a href="#">Health, Safety and Environment Test for Managers and Professionals Revision DVD</a>
									</h2>
									<p>This book is aimed at anyone who is about to take the CSCS CITB Touchscreen Managers and Professionals Test.</p>
								</div>
								<div class="course-content-bottom">
    								<div class="star-rating has-ratings" title="">
    									<span style="width:100%">
    										<button type="button" class="btn btn-sm btn-success" disabled>£20.00</button>
    									</span>
    								</div>
    								<a href="<?php echo 'applystdmtl.php?id=mgrdvd'; ?>" class="btn btn-sm btn-warning right-part">Buy Now</a>
    							</div>
							</div>
						</div>

					</div>

					<div class="sidebar col-lg-4">
						<div class="ads-widget widget" id="non-sticky">
								<a href="tel:0800-046-5506">
									<img src="images/cards/cscscard.jpg" alt="">
								</a>
							</div>

						<div class="products-widget widget" id="sticky"  >
							<h2>CITB Test</h2>
							<ul class="products-list">
							    <?php
                                    include("connect.php");
                					$sql = mysqli_query($conn, "SELECT * from test");
                					$row = mysqli_num_rows($sql);
                					while ($row = mysqli_fetch_array($sql)){ ?>
								<li>
									<a href="<?php echo 'applytest.php?id='.$row['id']; ?>"><img src="images/<?php echo $row['image']; ?>" alt=""></a>
                                    <div class="list-content">
                                    <h3><?php echo $row['test_type']; ?></h3>
                                    <a style="font-size:12px" href="<?php echo 'applytest.php?id='.$row['id']; ?>">Apply Now</a>
                                    </div>
								</li>
								<?php } ?>
							</ul>
						</div>
						<div class="products-widget widget">
							<h2>CSCS Cards</h2>
							<ul class="products-list">
							    <?php
                                    include("connect.php");
                					$sql = mysqli_query($conn, "SELECT * from cards_list WHERE id IN (1,3,7,8,10,11)");
                					$row = mysqli_num_rows($sql);
                					while ($row = mysqli_fetch_array($sql)){ ?>
								<li>
									<a href="<?php echo 'applycard.php?id='.$row['id']; ?>"><img src="images/cards/<?php echo $row['image']; ?>" alt=""></a>
                                    <div class="list-content">
                                    <h3><?php echo $row['name']; ?></h3>
                                    <a style="font-size:12px" href="<?php echo 'applycard.php?id='.$row['id']; ?>">Apply Now</a>
                                    </div>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>

				</div>
						
			</div>
		</section>
		<!-- End courses section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p>We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
							
							
						    
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	
	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src="js/script.js"></script>
	
	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    
</body>
</html>