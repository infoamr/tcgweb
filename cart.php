<?php
require_once(__DIR__ . "/includes/payzone_gateway.php");
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Takepayments Gateway - Cart</title>
  <meta name="description" content="Payment Gateway example integration">
  <meta name="author" content="Keith Rigby - Takepayments">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!--Takepayments CSS -->
  <link rel="stylesheet" href="assets/payzone_gateway.css?v=1.0.1">
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<body>
  <form class='payzone-form' id='shopping-cart-form' name='shopping-cart-form' action='<?php echo $PayzoneGateway->getURL('payment-page'); ?>' method='POST'>
    <?php #Check if Takepayments images should be displayed
    if ($PayzoneGateway->getPayzoneImages()) { ?>
      <div class='payzone-form-section'>
        <a href='https://www.takepayments.com/' target="_blank">
          <img class='payzone-logo' src="<?php echo $PayzoneHelper->getSiteSecureURL('base'); ?>/assets/images/payzone_logo.png" />
        </a>
      </div>
    <?php
    } ?>
      <div class='payzone-form-section'>
        <p class='payzone-form-header'>Shopping Cart Details</p>
      </div>
      <div class='payzone-form-section'>
          <?php $testprice = ltrim($_POST["testprice"], '£'); ?>
        <label for='FullAmount'>FullAmount:- <?php echo (isset($_POST["testprice"]))?$testprice:null; ?></label>
        <input type="hidden" name="FullAmount" value="<?php echo (isset($_POST["testprice"]))?$testprice:null; ?>" />
        <label for='OrderId'>OrderID:- <?php echo time(); ?></label>
        <input type="hidden" name="OrderID" value="<?php echo time(); ?>" />
        <?php $exam = (isset($_POST["exam"])) ? $_POST["exam"] : ''; ?>
        <label for='OrderDescription'>OrderDescription:- <?php echo (isset($_POST["test_type"])) ?$_POST["test_type"].' - '.$exam:null; ?></label>
        <input type="hidden" name="OrderDescription" value="<?php echo (isset($_POST["test_type"])) ?$_POST["test_type"].' - '.$exam:null; ?>"/>
        <input type="hidden" name="TransactionDateTime" value="<?php echo date('Y-m-d H:i:s P'); ?>" />
      </div>
      <hr>
      <div class='payzone-form-section'>
        <p class='payzone-form-header'>Customer Details</p>
      </div>
      <div class='payzone-form-section'>
        <label for='CustomerName'>CustomerName</label>
        <input type="text" name="CustomerName" value="<?php echo (isset($_POST["first_name"]))?$_POST["first_name"].' '.$_POST["last_name"]:null; ?>" />
        <label for='Address1'>Address1</label>
        <input type="text" name="Address1" value="<?php echo (isset($_POST["address"]))?$_POST["address"]:null; ?>" />
        <label for='Address2'>Address2</label>
        <input type="text" name="Address2" value="<?php echo (isset($_POST["address2"]))?$_POST["address2"]:null; ?>" />
        <label for='City'>City</label>
        <input type="text" name="City" value="<?php echo (isset($_POST["town"]))?$_POST["town"]:null; ?>" />
        <label for='State'>State</label>
        <input type="text" name="State" value="<?php echo (isset($_POST["state"]))?$_POST["state"]:null; ?>" />
        <label for='PostCode'>PostCode</label>
        <input type="text" name="PostCode" value="<?php echo (isset($_POST["postcode"]))?$_POST["postcode"]:null; ?>" />
        <label for='Country'>Country:- United Kingdom</label>
        <!--<input type="hidden" name="Country" value="United Kingdom" />-->
          <select name="Country">
            <option value="United Kingdom">United Kingdom</option>
          </select>
      </div>
    <hr>
    <span id='form_errors'></span>
    <div class='payzone-form-section'>
   
      <input id='payzone-cart-confirmation' type="checkbox" name="termsandconditions" class="termsandconditions" />
      <label for='termsandconditions' name="termsandconditions-label" class="termsandconditions-label" >Please accept terms and conditions before proceeding</label>
      
    </div>
    <div class='payzone-form-section'>
      <input id='payzone-cart-submit' type="submit" name="Submit" value="Submit" />
    </div>
    <?php
    if ($PayzoneGateway->getPayzoneImages()) { ?>
      <div class='payzone-form-section'>
        <a href="https://www.payzone.co.uk/" target="_blank">
          <img class='payzone-footer-image' src="<?php echo $PayzoneHelper->getSiteSecureURL('base'); ?>/assets/images/payzone_cards_accepted.png" />
        </a>
      </div>
      <?php
    } ?>
  </form>
  <?php #include scripts for handling of JSON Data
$page='cart';
require_once(__DIR__ . "/includes/helpers/payzone_scripts.php"); ?>
</body>
</html>
