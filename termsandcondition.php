<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Book Your CSCS & CITB Health, Safety & Environment Test Online | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="canonical" href="https://www.theconstructionguide.co.uk" />
	<script src="https://code.iconify.design/2/2.0.3/iconify.min.js"></script>
	<style>
	    p{
	        color:#000000;
	    }
	    .title p{color:#000000!important;}
	</style>
</head>
<body>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p><i class="material-icons">phone</i><a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i><a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.php">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="page-banner-section">
			<div class="container">
				<h1>Terms & Condition</h1>
				<ul class="page-depth">
					<li><a href="index.php">Home</a></li>
					<!--<li><a href="courses.php">Courses</a></li>-->
					<li><a href="termsandcondition.php">Terms & Condition</a></li>
				</ul>
			</div>
		</section>
		<!-- End page-banner-section -->

		<!-- single-course-section 
			================================================== -->
		<section class="single-course-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="single-course-box">
					        <!-- single top part -->
							<!--<div class="product-single-top-part">-->
								
							<!--	<div class="course-single-gallery">-->
							<!--		<img src="upload/courses/4.jpg" alt="">-->
							<!--	</div>-->
							<!--</div>-->

							<!-- single course content -->
							<div class="single-course-content">
							   <h2>Construction Guide Terms & Condition</h2>
							   <p style="text-align:justify">We would request you to go through our terms and conditions before making any appointments with The Construction Guide. Our terms and conditions are material to any organization or person that makes single (individual) or different appointments with Constructions Care. If you have any issue with our term of use, kindly refrain from using the website.</p>
								<p style="text-align:justify">The CITB test, CSCS Cards and Courses are organized services offered by theconstructionguide.co.uk that is owned by CHOOSEURFOOD LTD registered in England, under the company number 11651022. For the same, the registered office is 76 Coronation Street, Salford, England, M5 3SA. We use THECONSTRUCTIONGUIDE.CO.UK, as our trade name.</p>
								<p style="text-align:justify">Once you start using the website, it will indicate that you agreed to comply with the terms and conditions.</p>
								<p style="text-align:justify">The Construction Guide keeps all the authority to alter or change any of the Terms and conditions at whichever point it is needed, without giving notification to the user.</p>
								<p style="text-align:justify">Any new terms made or existing terms will apply just to the appointments made after the new terms are added to the site and appointments strategy. The terms on the Constructions guide site when you place your booking will be the terms which will be of your concern.</p>
								<p style="color:#ff5900"><b>Before becoming our client it is necessary that you agree to our term and conditions prior booking.</b></p>
								<p><b>Age Restrictions</b></p>
								<p>We can't book tests for any people younger than 16.</p>
								<p><b>Booking for a Business</b></p>
								<p>If you are using our service on behalf of a business that you work for, by paying for our services you confirm that you have the authority given from the business owner and they have authorized you to make payment for the services offered on our Construction Guide website.</p>
								<p><b>Installment Currencies</b></p>
								<p>All installments are handled in GBP.</p>
								<p><b>Term and Conditions of Bookings</b></p>
								<p>All bookings that are made are on 'subject to availability' basis. Our team will always endeavor to secure you the time and date that you requested, however, this cannot always be achieved. In the rare cases where we cannot secure you the date and time that you requested, our team will always look to book the closest available time and date.</p>
								<p>We will not be held liable for any loss of earnings or expenses that may be incurred by an individual or organization in order to book, as testing and scheduling may be cancelled or void at last minute due to unforeseen circumstances, including but not limited to, issue at the centre where testing/training cannot be done, closure of a testing centre, issue with equipment, issue relating to identification of the individual.</p>
								<p><b>Booking Confirmation.</b></p>
								<p>The Construction Guide sends a confirmation e-mail and text message on you device within 48 working hours once after the booking is placed. The client will likewise get confirmation of booking from CITB to assure their date and time. In any situation where the client has not received any response from The Construction guide within 48 hours, they can bring that to attention by mailing us on the info@theconstructionguide.co.uk</p>
								<p><b>Rescheduling or Cancelling a Booking</b></p>
								<p>You can reschedule your test before it takes place when the request is made at minimum of three working days prior to the scheduled test date. The Construction Guide will reschedule tests free of charge in these cases. Any requests to reschedule inside 72 hours of the scheduled test date & time will result in a new booking being required and an additional testing fee apply. Please note that if you wish to reschedule your test within 72 hours of the original test date that we provided, we have to wait 48 hours until your original test date has passed.<br>Scratch-off solicitations are dependent upon a £10.00 organization and undoing charge; this expense covers and incorporates, yet isn't restricted to, correspondence with the applicant, dropping of mentioned tests with CITB and the discount cycle.</p>
								<p><b>Verification of Bookings</b></p>
								<p>In the wake of setting your booking you will get an affirmation email sent by us, which goes about as a receipt of installment and will fill in as verification of the booking and installment made, alongside your full acknowledgment, everything being equal, and conditions as itemized here.<br>We don't acknowledge obligation if installment is declined by the credit/check card provider.</p>
								<p><b>Booking Your Test through Constructions Guide</b></p>
								<p>By booking your test with us, you completely recognize that your booking is for the course of action of the test and not for the date that you chose (which are constantly offered 'subject to accessibility'). The charge that you pay is comprehensive of the £21 test expense and our booking and backing expense.<br>The subtleties that you give us when rounding out the structure will be utilized to deal with your booking. We are not mindful if your booking is put with any mistaken subtleties that you have given us. On the off chance that anntery mistake is made for our benefit, for example, erroneously presenting the specific subtleties that you have given us, it is our obligation and we won't charge for any corrections.<br>The Constructions Guide group are entrusted to audit any incorrectness' for each reserving submitted, in the event that we require any extra data from you, or accept that you may have booked some unacceptable test or test focus, we will reach you through email to affirm that every one of your subtleties are right before booking your test. If it's not too much trouble reach us if there are any mistakes or mistaken subtleties in your request at info@theconstructionguide.co.uk</p>
								<p>Your installment is completely comprehensive of the expense for the test, alongside an organization charge. As an outsider booking specialist, we charge an expense for the administration we give. Our administration incorporates the accompanying: </p>
								<p>Your payment is fully inclusive of the fee for the test, along with an administration fee. As a third party booking agent, we charge a fee for the management service we provide. Our service includes the following:</p>
								<p>Evaluating each reserving to guarantee that our clients subtleties seem right and they are reserving onto the right test.</p>
								<p>Making sure about the time, date and setting mentioned by the client (or closest date/time to that mentioned) for each reserving.</p>
								<p>Reaching the client and advising them regarding their affirmed test dates through email and instant message.</p>
								<p>Managing any solicitations to reschedule a clients test and activity retractions when required. </p>
								<p>The value that you pay for our booking administration will be the value that is shown booking stage on the site. Developments Care requires the full cleared installment before we can book your test. </p>
								<p>The legal rights that you have as a shopper are not influenced by these terms and conditions.</p>
								<p>Different rights you have while setting a booking through The Constructions Guide. <br>The option to drop your test </p>
								<p>You reserve an option to drop a booked test and get a discount. On the off chance that you wish to drop your test, you have to get in touch with us by means of the email below:</p>
								<p>
									<a href="info@theconstructionguide.co.uk"></a>
									<a href="info@theconstructionguide.co.uk">info@theconstructionguide.co.uk</a>
								</p>
								<p>You will be needed to give us the accompanying subtleties:</p>
								<p>Your complete name and booking reference number.</p>
								<p>At any rate 3 elective test dates that you are accessible</p>
								<p>To guarantee that we can reschedule your test as expected, we require these subtleties at any rate 3 working days before your test date. In the event that you neglect to reach us with the subtleties that we have delineated, we will be not able to reschedule your test until 48 hours after the first test date has passed.</p>
								<p>If you wish to reschedule the course venue, this may result in additional fees which you will be obliged to cover before we can reschedule the venue.</p>
								<p>By consenting to these Terms specified here, you hence consent to the conditions of our Privacy Policy. Kindly observe Privacy Policy – more subtleties.</p>
								<p>By consenting to these Terms specified here, you consequently consent to the particulars of our Privacy Policy.</p>
								<p>One Day Courses Booking Policy</p>
								<p>The Construction Guide claims all authority to drop or adjust the Course Dates or the arrangement of Services or the Location and the individual or the association offering the Support or make sensible varieties to the courses without earlier notification. In function of undoing, the booking will regularly be moved to the following accessible Course except if the Customer explicitly demands in any case.</p>
								<p>Undoing, reschedules and non-participation strategy:</p>
								<p>You will be qualified for a discount where the mentioned abrogation is inside (31) days of the Course Date; to which crossing out solicitations are dependent upon a £10.00 organization and scratch-off charge; this expense covers and incorporates, yet isn't restricted to, correspondence with the competitor, scratch-off of mentioned course(s) with the preparation supplier and the discount cycle.</p>
								<p>Course once reserved won't be dropped.</p>
								<p>On the off chance that you wish to reschedule a course you have to give us at any rate 31 days notice. Inability to do this and not going to the course will bring about relinquishing the course expense.</p>
								<p>On the off chance that you wish to reschedule the course scene, this may bring about extra expenses which you will be obliged to cover before we can reschedule the setting.</p>
								<p>The Construction Guide won't be considered liable under any conditions that an applicant can't go to the course-models incorporate terrible climate, vehicle stall or whatever other explanation that may prevent the up-and-comer from going to the day course.</p>								
								<!-- course section -->
							</div>
							<!-- end single course content -->
						</div>                        
					</div>
				</div>						
			</div>
		</section>
		<!-- End single-course section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p>We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
							
							
						    
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src="js/script.js"></script>

	<script>
		var tpj=jQuery;
		var revapi202;
		tpj(document).ready(function() {
			if (tpj("#rev_slider_202_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_202_1");
			} else {
				revapi202 = tpj("#rev_slider_202_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "js/",
					dottedOverlay: "none",
					delay: 5000,
					navigation: {
						keyboardNavigation: "off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation: "off",
						onHoverStop: "off",
						arrows: {
					        enable: true,
					        style: 'gyges',
					        left: {
					            container: 'slider',
					            h_align: 'left',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        },
					 
					        right: {
					            container: 'slider',
					            h_align: 'right',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        }
					    },
						touch: {
							touchenabled: "on",
							swipe_threshold: 75,
							swipe_min_touches: 50,
							swipe_direction: "horizontal",
							drag_block_vertical: false
						},
						bullets: {
 
					        enable: false,
					        style: 'persephone',
					        tmp: '',
					        direction: 'horizontal',
					        rtl: false,
					 
					        container: 'slider',
					        h_align: 'center',
					        v_align: 'bottom',
					        h_offset: 0,
					        v_offset: 55,
					        space: 7,
					 
					        hide_onleave: false,
					        hide_onmobile: false,
					        hide_under: 0,
					        hide_over: 9999,
					        hide_delay: 200,
					        hide_delay_mobile: 1200
 						}
					},
					responsiveLevels: [1210, 1024, 778, 480],
					visibilityLevels: [1210, 1024, 778, 480],
					gridwidth: [1210, 1024, 778, 480],
					gridheight: [700, 700, 600, 600],
					lazyType: "none",
					parallax: {
						type: "scroll",
						origo: "slidercenter",
						speed: 1000,
						levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
						type: "scroll",
					},
					shadow: 0,
					spinner: "off",
					stopLoop: "off",
					stopAfterLoops: -1,
					stopAtSlide: -1,
					shuffle: "off",
					autoHeight: "off",
					fullScreenAutoWidth: "off",
					fullScreenAlignForce: "off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "0px",
					disableProgressBar: "on",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll: "off",
						nextSlideOnWindowFocus: "off",
						disableFocusListener: false,
					}
				});
			}
		}); /*ready*/
	</script>	

	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>