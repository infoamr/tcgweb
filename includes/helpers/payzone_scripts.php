<?php
/**
* Takepayments Payment Gateway
* ========================================
* Web:   http://takepayments.com
* Email:  online@takepayments.com
* Authors: Takepayments, Keith Rigby
*/

if (count(get_included_files()) ==1) {
    exit("Direct access not permitted.");
}

if($page=='payment' || $page=='cart' || $page=='results'){
  #Payzone Modal Form functions
  require_once(__DIR__ .'/../templates/payzone-modal.php');
}

if($page=='payment'){
?>
  <script>
    PayzonePaymentForm = document.getElementById('payzone-payment-form');
    function payzonePaymentPageLoad(){
      <?php
      if ($PayzoneGateway->getIntegrationType() == \Payzone\Constants\INTEGRATION_TYPE::HOSTED){ ?>
        <?php
        if($PayzoneGateway->getHostedIframe()){  ?>
          window.parent.postMessage({'option':'modalsize','value':'5'},"<?php echo $PayzoneHelper->getSiteSecureURL('root'); ?>");
          window.parent.postMessage({'option':'iframesrc','value':'payment'},"<?php echo $PayzoneHelper->getSiteSecureURL('root'); ?>");
          <?php
        }
        if (!$PayzoneGateway->getDebugMode()) { ?>
          setTimeout(function() {PayzonePaymentForm.submit();},1000);
          <?php
        }
        else {  ?>
          document.getElementById('payzone-loading-wrap').style.display='none';
          document.getElementById('payzone-payment-form').style.display='block';
          <?php
        }
        if($PayzoneGateway->getHostedIframe()){  ?>
          window.parent.postMessage({'option':'modalsize','value':'66'},"<?php echo $PayzoneHelper->getSiteSecureURL('root'); ?>");
          <?php
        }
      }
      ?>
    }
  </script>
<?php
}

?>
<script src="assets/payzone_validate.js"></script>
<?php
### Hosted payment form cart submission control
if($PayzoneGateway->getIntegrationType()==\Payzone\Constants\INTEGRATION_TYPE::HOSTED) {
  if($page=='cart'){
    ?>
    <script>
      document.getElementById("payzone-cart-submit").addEventListener("click",payzoneCartFormSubmission );
      function payzoneCartFormSubmission(evt){
        evt.preventDefault();
        var cartForm    = document.getElementById('shopping-cart-form');
        var validated   = validateForm(cartForm,"hosted","<?php echo $PayzoneHelper->boolToString($PayzoneGateway->getHostedCustomerDetails()); ?>");
        if(validated){
          <?php
          ### Conditionally load iFrame functionality if option selected
          if( $PayzoneGateway->getHostedIframe()) { ?>
            var iframepage='cart';
            document.getElementById("payzone-payment-modal-background").addEventListener("click", closePayzoneModal);
            document.getElementById("payzone-modal-close").addEventListener("click", closePayzoneModal);
            window.addEventListener("message", receiveMessageCart, false);
            var ifrm = document.createElement("iframe");
            ifrm.setAttribute("id", "payzone-iframe");
            ifrm.setAttribute("name", "payzone-iframe");
            ifrm.setAttribute("src", "<?php echo $PayzoneGateway->getURL('loading-page'); ?>");
            ifrm.setAttribute("scrolling", "none");
            ifrm.setAttribute("frameborder", "none");
            pzgModal.appendChild(ifrm);
            openPayzoneModal(5);
            document.getElementById('shopping-cart-form').target = 'payzone-iframe';
            <?php
          } ?>
          document.getElementById('shopping-cart-form').submit();
        }
      }
    </script>
    <?php
  }
  else if ($page=='payment'){
    ?>
    <?php
  }
}
?>
