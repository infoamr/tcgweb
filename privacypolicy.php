<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Book Your CSCS & CITB Health, Safety & Environment Test Online | Construction Guide UK</title>
	<meta name= "description" content="Looking for CSCS card & CSCS Courses online? Grab the best deal on a CSCS card, NVQ Courses, CIBT Tests & various Health and safety courses. Hurry Up! reach us at 0800-046-5506">
    <meta name="keywords" content="cscs health and safety mock test, cscs exam registration, cscs card website, cscs card questions and answers, cscs card apply for card online, apply for a cscs card labourer, book health and safety test cscs." >

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/studiare-assets.min.css">
	<link rel="stylesheet" type="text/css" href="css/fonts/font-awesome/font-awesome.min.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/elegant-icons/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/fonts/iconfont/material-icons.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="canonical" href="https://www.theconstructionguide.co.uk" />
	<script src="https://code.iconify.design/2/2.0.3/iconify.min.js"></script>
	<style>
	    p{
	        color:#000000;
	    }
	    .title p{color:#000000!important;}
	</style>

</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">

			<div class="top-line">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p><i class="material-icons">phone</i><a href="tel:0800-046-5506"><b><span style="color:#ffffff">0800-046-5506</span></b></a></p>
							<p><i class="material-icons">email</i><a href="mailto: info@theconstructionguide.co.uk"> <b><span style="color:#ffffff"> info@theconstructionguide.co.uk
                            </span></b></a></p>
						</div>
						
					</div>
				</div>
			</div>

			<form class="search_bar">
				<div class="container">
					<input type="search" class="search-input" placeholder="What are you looking for...">
					<button type="submit" class="submit">
						<i class="material-icons">search</i>
					</button>
				</div>
			</form>

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">

					<a class="navbar-brand" href="index.php">
						<img src="images/logo.svg" alt="">
					</a>

					<a href="#" class="mobile-nav-toggle"> 
						<span></span>
					</a>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
                            <li class="drop-link"><a class="active" href="index.php"><strong>Home</strong></a></li>
                            <li class="drop-link"><a href="cscscards.php"><strong>CSCS Cards</strong></a></li>
                            <li class="drop-link"><a href="citbtest.php"><strong>CITB Test</strong></a></li>
                            <li class="drop-link"><a href="courses.php"><strong>Safety Courses</strong></a></li>
                            <li class="drop-link"><a href="nvqcourses.php"><strong>NVQ Courses</strong></a></li>
                            <li class="drop-link"><a href="studymaterial.php"><strong>Study Material</strong></a></li>
                            <li><a href="contact.php"><strong>Contact</strong></a></li>
                        </ul>
						<a href="https://tcg.abyzit.com/admin/authentication" target="_blank" class="register-modal-opener login-button" style="margin-right:-40px!important"><i class="material-icons">perm_identity</i> Staff Login</a>
					</div>
				</div>
			</nav>

			<div class="mobile-menu">
				<div class="search-form-box">
					<form class="search-form">
						<input type="search" class="search-field" placeholder="Enter keyword...">
						<button type="submit" class="search-submit">
							<i class="material-icons open-search">search</i> 
						</button>
					</form>
				</div>
				
				<nav class="mobile-nav">
					<ul class="mobile-menu-list">
						<li><a href="index.php">Home</a></li>
						<li class="drop-link"><a href="cscscards.php">CSCS Cards</a></li>
						<li class="drop-link"><a href="citbtest.php">CITB Test</a></li>
						<li class="drop-link"><a href="courses.php">Safety Courses</a></li>
						<li class="drop-link"><a href="nvqcourses.php">NVQ Courses</a></li>
						<li class="drop-link"><a href="studymaterial.php">Study Material</a></li>
						<li class="drop-link"><a href="contact.php">Contact</a></li>
					</ul>
				</nav>
			</div>

		</header>
		<!-- End Header -->

		<!-- page-banner-section 
			================================================== -->
		<section class="page-banner-section">
			<div class="container">
				<h1>Privacy Policy</h1>
				<ul class="page-depth">
					<li><a href="index.php">Home</a></li>
					<!--<li><a href="courses.php">Courses</a></li>-->
					<li><a href="privacypolicy.php">Privacy Policy</a></li>
				</ul>
			</div>
		</section>
		<!-- End page-banner-section -->

		<!-- single-course-section 
			================================================== -->
		<section class="single-course-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="single-course-box">
					        <!-- single top part -->
							<!--<div class="product-single-top-part">-->
								
							<!--	<div class="course-single-gallery">-->
							<!--		<img src="upload/courses/4.jpg" alt="">-->
							<!--	</div>-->
							<!--</div>-->

							<!-- single course content -->
							<div class="single-course-content">
							    <h2>Construction Guide Privacy Policy</h2>
							    <p style="text-align:justify">The European Union General Data Protection Regulation (EU GDPR) replaces the UK Data Protection Act 1998 and EU Directive 95/46/EC. GDPR plans to forestall security breaks and loss of Personally Identifiable Information (PII) by associations that hold or cycle such information by giving a rational and careful individual information protection law across EU part states.</p>
								<p style="text-align:justify">Development Care requires each push to consent to GDPR guidelines and our security strategy has been arranged to give you an away from of how we gather, use, ensure or in any case handle your actually recognizable data as per our site while keeping up and advising you regarding your privileges.</p>
								<h2>What is Personal Identifiable Information? (PII)</h2>
								<p style="text-align:justify">Individual Identifiable Information is the individual information or data that can be utilized all alone or with other data to distinguish, contact, or find a solitary individual, or to recognize a person in setting. Models incorporate a person's name, age, address, date of birth, their sex and contact subtleties. PII may likewise contain Special class information or delicate data, for example, a person's wellbeing, racial or ethnic source, political conclusions, strict or philosophical convictions, worker's organization enrollment, hereditary and biometric information, or sexual direction.</p>
								
								<div class="course-section">
									<h6>For us to convey our products and ventures we require the accompanying data: :-</h6>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Name</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Address</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Date of Birth</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Telephone number</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>E-mail address</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>IP Address (your unique online electronic identifier)</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Unique identifier numbers allocated by the Construction Industry Training Board (CITB).</p>
												</div>
											</div>
										</div>
										
									</div>
								</div>
								
								<div class="course-section">
									<h6>Your Rights and how to Exercise them :-</h6>
									<p>Concerning your Personal recognizable data (PII) you have the accompanying legitimate rights:</p>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>The option to be educated how your PII in prepared. (where it is put away and for what reason)</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>The option to get to your PII .</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> The option to protest as well or confine the preparing of your PII.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>The option to erase your PII.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>The option to get an electronic duplicate of your PII.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> The option to redress or correct your PII whenever definite put away are erroneous.</p>
												</div>
											</div>
										</div>
										
										
										
									</div>
								</div>
								
								<p>You can practice your privileges whenever and to do so the guidelines expect you to present a subject access demand (SAR) to our information insurance delegate (DPR) at info@theconstructionguide.co.uk There is no charge or expense to handle these solicitations except if they are viewed as monotonous, entirely unwarranted or potentially unnecessary. So, all things considered we would be qualified for charge a sensible authoritative expense. In the far-fetched function we can't completely meet any of your solicitations because of uncontrollable issues at hand, we will build up opportune and steady open lines of correspondence as specified inside the guidelines.</p>
                                <h2>Installment subtleties</h2>
								<p>Developments Care don't take installments legitimately. Whenever you have affirmed your buy you will be coordinated to our safe installment trader. Developments Care do no store or approach your installment subtleties. Our installment vendor protection strategy can be gotten to on their site.</p>
								
								<!-- course section -->
								<div class="course-section">
									<h6>How we gather Data :-
									<p>We gather Data in the accompanying manners:</p>
									</h6>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> information is given to us by you ;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>information is gotten from different sources; and</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> information is gathered consequently.</p>
												</div>
											</div>
										</div>
										<p>Information that is given to us by you</p>
									</div>
								</div>
								
								<div class="course-section">
									<h6>Developments Care Ltd. will gather your Data in various manners, for instance:-</h6>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>at the point when you get in touch with us through the Website, by phone, post, email or through some other methods;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>at the point when you complete overviews that we use for research purposes (in spite of the fact that you are not obliged to react to them);</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>at the point when you make installments to us, through this Website or something else;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>at the point when you utilize our administrations;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>CITB;</p>
												</div>
											</div>
										</div>
										<p>for each situation, as per this security strategy.</p>
									</div>
								</div>
								<h6>Information that is gotten from outsiders</h6>
								<p>Developments Care Ltd. will may likewise get Data about you from the accompanying outsiders, for example,</p>
								<div class="course-section">
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>CITB;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> CSCS;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Your boss</p>
												</div>
											</div>
										</div>
										
										
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>Other outsider course/preparing suppliers.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<h6>Information that is gathered consequently</h6>
								<p>To the degree that you access the Website, we will gather your Data</p>
								<p>consequently, for instance:</p>
								<div class="course-section">
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">we consequently gather some data about your visit to the Website. This data encourages us to make upgrades to Website substance and route, and incorporates your IP address, the date, times and recurrence with which you access the Website and the manner in which you utilize and cooperate with its substance.</p>
												</div>
											</div>
										</div>
										
										
									</div>
								</div>
								
								<h6>Our utilization of Data</h6>
								<div class="course-section">
									<p>Any or the entirety of the above Data might be needed by us every now and then so as to give you the most ideal help and experience when utilizing our Website. In particular, Data might be utilized by us for the accompanying reasons::-</p>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>interior record keeping;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p> improvement of our items/administrations;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p>transmission by email of promoting materials that might hold any importance with you;</p>
												</div>
											</div>
										</div>
										<p>for each situation, as per this security strategy.</p>
									</div>
								</div>
								
								<p>We may utilize your Data for the above purposes on the off chance that we esteem it important to do as such for our genuine advantages. In the event that you are not happy with this, you reserve the privilege to protest in specific conditions (see the segment headed "Your privileges" beneath).</p>
								<div class="course-section">
									<h6>For the conveyance of direct advertising to you through email, we'll need your assent, regardless of whether by means of a select in or delicate pick in:-</h6>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">delicate select in assent is a particular sort of assent which applies when you have recently drawn in with us (for instance, you reach us to approach us for additional insights concerning a specific item/administration, and we are advertising comparable items/administrations). Under "delicate pick in" assent, we will accept your assent as given except if you quit.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">for different kinds of e-promoting, we are needed to acquire your express assent; that is, you have to make positive and agreed move while consenting by, for instance, checking a mark box that we'll give.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;"> on the off chance that you are not fulfilled about our way to deal with advertising, you reserve the privilege to pull back assent whenever. To discover how to pull back your assent, see the segment headed "Your privileges" underneath.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="course-section">
									<h6>Who we share Data with:-</h6>
									<p>We may impart your Data to the accompanying gatherings of individuals for the accompanying reasons:</p>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">any of our gathering organizations or members – *** client data imparted to amass organizations or partners to deal with the oders ***;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">our representatives, specialists as well as expert counselors so they can handle your requests *** client data imparted to workers, operators and additionally proficient consultants ***;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">outsider specialist co-ops who offer types of assistance to us which require the preparing of individual data,to permit them to handle your request, for example, CITB for CITB Tests, CSCS for CSCS Cards, NVQ/Course suppliers to book you on the course/NVQ mentioned; and course material supplier to give you quick and effective conveyance.- *** client data imparted to outsider specialist organizations **;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">outsider installment suppliers who measure installments made over the Website or via telephone – *** to empower them outsider installment suppliers to handle client your installments and discounts ***;</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">for each situation, as per this security strategy</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="course-section">
									<h6>Keeping Data secure:-</h6>
									<p>We will utilize specialized and hierarchical measures to defend your Data, for instance:</p>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">admittance to your record is constrained by a secret word and a client name that is novel to you.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">we store your Data on secure workers.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">installment subtleties are scrambled utilizing SSL innovation (ordinarily you will see a lock symbol or green location bar (or both) in your program when we utilize this innovation.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<h6>We are confirmed to PCI DSS. This group of norms encourages us deal with your Data and keep it secure.</h6>
								<p>Specialized and hierarchical measures incorporate measures to manage any presumed information break. In the event that you presume any abuse or misfortune or unapproved admittance to your Data, if it's not too much trouble let us know promptly by reaching us by means of this email address:info@theconstructionguide.co.uk</p>
								<p>In the event that you need itemized data from Get Safe Online on the most proficient method to secure your data and your PCs and gadgets against extortion, wholesale fraud, infections and numerous other online issues, if it's not too much trouble visit www.getsafeonline.org. Get Safe Online is upheld by HM Government and driving organizations.</p>
								<h6>Information maintenance</h6>
								<p>Except if a more drawn out maintenance period is required or allowed by law, we will just hold your Data on our frameworks for the period important to satisfy the reasons laid out in this protection strategy or until you demand that the Data be erased. Regardless of whether we erase your Data, it might continue on reinforcement or chronicled media for legitimate, charge or administrative purposes.</p>
								
								<div class="course-section">
									<h6>Your privileges:-</h6>
									<p>You have the accompanying rights comparable to your Data:</p>
									<div class="panel-group">
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Option to get to – the option to ask for (I) duplicates of the data we hold about you whenever, or (ii) that we alter, refresh or erase such data. On the off chance that we give you admittance to the data we hold about you, we won't charge you for this, except if your solicitation is "obviously unwarranted or unreasonable." Where we are lawfully allowed to do as such, we may deny your solicitation. On the off chance that we deny your solicitation, we will disclose to you the reasons why.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Option to address – the option to have your Data amended on the off chance that it is mistaken or deficient.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Option to eradicate – the option to demand that we erase or eliminate your Data from our frameworks.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Option to confine our utilization of your Data – the option to "block" us from utilizing your Data or breaking point the manner by which we can utilize it.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Right to information versatility – the option to demand that we move, duplicate or move your Data.</p>
												</div>
											</div>
										</div>
										
										<div class="course-panel-heading" style="cursor: none!important;">
											<div class="panel-heading-left">
												<div class="course-lesson-icon">
													<i class="fa fa-angle-right"></i>
												</div>
												<div class="title">
													<p style="line-height: 1.6;">Option to protest – the option to have a problem with our utilization of your Data including where we use it for our real advantages.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<p>To make enquiries, practice any of your privileges set out above, or pull back your agree to the preparing of your Data (where assent is our lawful reason for handling your Data), if you don't mind reach us through this email address:info@theconstructionguide.co.uk</p>
								<p>On the off chance that you are not happy with the manner in which a grievance you make according to your Data is taken care of by us, you might have the option to allude your objection to the pertinent information insurance authority. For the UK, this is the Information Commissioner's Office (ICO). The ICO's contact subtleties can be found on their site at https://ico.org.uk/.</p>
								<p>It is significant that the Data we hold about you is precise and current. Kindly keep us educated if your Data changes during the period for which we hold it.</p>
								
								<h6>Moves outside the European Economic Area</h6>
								<p>Information which we gather from you might be put away and prepared in and moved to nations outside of the European Economic Area (EEA). For instance, this could happen if our workers are situated in a nation outside the EEA or one of our specialist co-ops is arranged in a nation outside the EEA. We likewise share data with our gathering organizations, some of which are situated external the EEA.</p>
								<p>We will just exchange Data outside the EEA where it is consistent with information insurance enactment and the methods for move gives satisfactory protections comparable to your information, eg by method of information move understanding, consolidating the current standard authoritative provisos received by the European Commission, or by joining to the EU-US Privacy Shield Framework, if the association in receipt of the Data is situated in the United States of America.</p>
								<p>To guarantee that your Data gets a satisfactory degree of security, we have set up fitting protections and methods with the outsiders we share your Data with. This guarantees your Data is treated by those outsiders in a manner that is reliable with the Data Protection Laws.</p>
								
								<h6>Connections to different sites</h6>
								<p>This Website may, now and again, give connects to different sites. We have no influence over such sites and are not answerable for the substance of these sites. This protection strategy doesn't reach out to your utilization of such sites. You are encouraged to peruse the security strategy or explanation of different sites preceding utilizing them.</p>
								<h6>Changes of business proprietorship and control</h6>
								<p>Developments Care Ltd. may, now and again, grow or decrease our business and this may include the deal as well as the exchange of control of all or part of Constructions Care Ltd.. Information gave by Users will, where it is pertinent to any aspect of our business so moved, be moved alongside that part and the new proprietor or recently controlling gathering will, under the details of this security strategy, be allowed to utilize the Data for the reasons for which it was initially provided to us.</p>
								<p>We may likewise reveal Data to an imminent buyer of our business or any piece of it. In the above cases, we will make strides with the point of guaranteeing your security is ensured.</p>
								<h6>General</h6>
								<p>You may not move any of your privileges under this security strategy to some other individual. We may move our privileges under this protection strategy where we sensibly accept your privileges won't be influenced.</p>
								<p>On the off chance that any court or equipped position finds that any arrangement of this protection strategy (or some portion of any arrangement) is invalid, unlawful or unenforceable, that arrangement or part-arrangement will, to the degree required, be esteemed to be erased, and the legitimacy and authorize capacity of different arrangements of this security strategy won't be influenced.</p>
								<p>Except if in any case concurred, no postponement, demonstration or oversight by a gathering in practicing any privilege or cure will be considered a waiver of that, or some other, right or cure.</p>
								<p>This Agreement will be administered by and deciphered by the law of England and Wales. All questions emerging under the Agreement will be dependent upon the elite locale of the English and Welsh courts.</p>
								<h6>Changes to this security strategy</h6>
								<p>Developments Care Ltd. claims all authority to change this protection strategy as we may esteem important now and again or as might be legally necessary. Any progressions will be quickly posted on the Website and you are regarded to have acknowledged the conditions of the protection strategy on your first utilization of the Website following the adjustments. You may contact Constructions Care Ltd. by email at info@theconstructionguide.co.uk</p>
								<!-- end course section -->

								

								

								

							</div>
							<!-- end single course content -->

							

						</div>
                        
					</div>

					

				</div>
						
			</div>
		</section>
		<!-- End single-course section -->

		<!-- footer 
			================================================== -->
		<footer>
			<div class="container">

				<div class="up-footer">
					<div class="row">

						<div class="col-lg-4 col-md-6">
						    
							<div class="footer-widget text-widget">
								<a href="index.php" class="footer-logo"><img src="images/logo_light.svg" alt=""></a>
								<p>We offer innovative services for the construction workforce. TCG is dedicated to make the construction arena safer and more efficient.</p>
								<ul>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">location_on</i>
										</div>
										<div class="contact-info-value">Address:25 Sipson Road, West Drayton,<br> UB7 9DQ, London</div>
									</li>
									<li>
										<div class="contact-info-icon">
											<i class="material-icons">phone_android</i>
										</div>
										<div class="contact-info-value"><a href="tel:0800-046-5506"><span style="color:#ffffff">0800-046-5506</span></a></div>
									</li>
									
								</ul>
							</div>
							
							
						    
						    
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget quick-widget">
								<h2>Quick Links</h2>
								<ul class="quick-list">
									<li><a href="index.php">Home</a></li>
									<li><a href="cscscards.php">CSCS Cards</a></li>
									<li><a href="contact.php">Contact</a></li>
									<li><a href="citbtest.php">CITB Test</a></li>
									<li><a href="privacypolicy.php">Privacy Policy</a></li>
									<li><a href="nvqcourses.php">NVQ Courses</a></li>
									<li><a href="termsandcondition.php">Terms & Condition </a></li>
									<li><a href="courses.php">Safety Courses</a></li>
								</ul>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="footer-widget subscribe-widget">
								<h2>Newsletter</h2>
								<p>Don’t miss anything, sign up now and keep informed about our company.</p>
								<div class="newsletter-form">
									<input class="form-control" type="email" name="EMAIL" placeholder="Enter Your E-mail" required="">
									<input type="submit" value="Subscribe">
								</div>
							</div>
						</div>
						<img class="pay_image" src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">	
					</div>
				</div>
				<!--<img src="images/TP_Card_Logos_[Horiz]_AMEX_Full_Colour.png" style="background-color:#F8F9FA" alt width="100%" height="auto">-->
			</div>

			<div class="footer-copyright copyrights-layout-default"  style="padding-top :10px">
				<div class="container">
					<div class="copyright-inner">
						<div class="copyright-cell"> &copy; <?php echo date("Y"); ?> <span class="highlight">The Construction Guide</span>. Created by ABYZIT.COM.</div>
						<div class="copyright-cell">
							<ul class="studiare-social-links">
								<li><a href="https://www.facebook.com/The-Construction-Guide-100481089111368" target="_blank" class="facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://twitter.com/constru47515413" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<!--<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>-->
								<li><a href="https://www.linkedin.com/in/the-construction-guide-706b60224/" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->

	<script src="js/studiare-plugins.min.js"></script>
	<script src="js/jquery.countTo.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCiqrIen8rWQrvJsu-7f4rOta0fmI5r2SI&amp;sensor=false&amp;language=en"></script>
	<script src="js/gmap3.min.js"></script>
	<script src="js/script.js"></script>

	<script>
		var tpj=jQuery;
		var revapi202;
		tpj(document).ready(function() {
			if (tpj("#rev_slider_202_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_202_1");
			} else {
				revapi202 = tpj("#rev_slider_202_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "js/",
					dottedOverlay: "none",
					delay: 5000,
					navigation: {
						keyboardNavigation: "off",
						keyboard_direction: "horizontal",
						mouseScrollNavigation: "off",
						onHoverStop: "off",
						arrows: {
					        enable: true,
					        style: 'gyges',
					        left: {
					            container: 'slider',
					            h_align: 'left',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        },
					 
					        right: {
					            container: 'slider',
					            h_align: 'right',
					            v_align: 'center',
					            h_offset: 20,
					            v_offset: -60
					        }
					    },
						touch: {
							touchenabled: "on",
							swipe_threshold: 75,
							swipe_min_touches: 50,
							swipe_direction: "horizontal",
							drag_block_vertical: false
						},
						bullets: {
 
					        enable: false,
					        style: 'persephone',
					        tmp: '',
					        direction: 'horizontal',
					        rtl: false,
					 
					        container: 'slider',
					        h_align: 'center',
					        v_align: 'bottom',
					        h_offset: 0,
					        v_offset: 55,
					        space: 7,
					 
					        hide_onleave: false,
					        hide_onmobile: false,
					        hide_under: 0,
					        hide_over: 9999,
					        hide_delay: 200,
					        hide_delay_mobile: 1200
 						}
					},
					responsiveLevels: [1210, 1024, 778, 480],
					visibilityLevels: [1210, 1024, 778, 480],
					gridwidth: [1210, 1024, 778, 480],
					gridheight: [700, 700, 600, 600],
					lazyType: "none",
					parallax: {
						type: "scroll",
						origo: "slidercenter",
						speed: 1000,
						levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
						type: "scroll",
					},
					shadow: 0,
					spinner: "off",
					stopLoop: "off",
					stopAfterLoops: -1,
					stopAtSlide: -1,
					shuffle: "off",
					autoHeight: "off",
					fullScreenAutoWidth: "off",
					fullScreenAlignForce: "off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "0px",
					disableProgressBar: "on",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll: "off",
						nextSlideOnWindowFocus: "off",
						disableFocusListener: false,
					}
				});
			}
		}); /*ready*/
	</script>	

	<!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5fb6ae3ba1d54c18d8eb5c90/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</body>
</html>